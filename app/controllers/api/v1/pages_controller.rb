module Api::V1
  class PagesController < ApiController
    before_action :set_page, only: [:show, :destroy]

    def index
      render json: Page.all, each_serializer: IndexPageSerializer
    end

    def show
      render json: { page: @page, status: 200 }
    end

    def create
      @page = Page.new(page_params)
      if @page.save
        render json: {page: @page, status: 200}
      else
        render json: {errors: @page.errors.full_messages, status: 422 }
      end
    end

    def destroy
      @page.destroy
      render json: { status: 200 }
    end

    private

    def set_page
      @page = Page.find(params[:id])
    end

    def page_params
      params.require(:page).permit(:url)
    end
  end
end
