module Api::V1
  class ApiController < ApplicationController
    rescue_from ActiveRecord::RecordNotFound do
      render json: {errors: I18n.t('active_records.errors.record_not_found'), status: 404 }
    end
  end
end
