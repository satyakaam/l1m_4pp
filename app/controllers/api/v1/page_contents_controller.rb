module Api::V1
  class PageContentsController < ApiController
    before_action :set_page_content, only: [:show]

    def index
      render json: {page_contents: PageContent.all, status: 200}
    end

    def show
      render json: {page_content: @page_content, status: 200}
    end

    private

    def set_page_content
      @page_content = PageContent.find(params[:id])
    end
  end
end
