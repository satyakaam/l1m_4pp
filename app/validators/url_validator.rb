class UrlValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    begin
      unless Nokogiri::HTML(open(value))
        record.errors[attribute] << (options[:message] || I18n.t('active_records.errors.page.url'))
      end
    rescue
      record.errors[attribute] << (options[:message] || I18n.t('active_records.errors.page.url'))
    end
  end
end
