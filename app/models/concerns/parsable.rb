module Parsable
  extend ActiveSupport::Concern
  require 'nokogiri'
  require 'open-uri'

  included do
    after_create :create_page_content
  end

  def create_page_content
    doc = Nokogiri::HTML(open(url))

    [doc.css('a'), doc.css('h1'), doc.css('h2'), doc.css('h3')].each do |tags|
      tags.each do |tag|
        page_contents.create(content_type: tag.name, content: tag.inner_text.force_encoding('iso8859-1').encode('utf-8'))
      end
    end
  end
end
