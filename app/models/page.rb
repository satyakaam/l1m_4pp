class Page < ApplicationRecord
  include Parsable

  has_many :page_contents, dependent: :destroy
  validates :url, presence: true, url: true
end
