class IndexPageSerializer < PageSerializer
  def page_contents
    object.page_contents.group(:content_type).count
  end
end
