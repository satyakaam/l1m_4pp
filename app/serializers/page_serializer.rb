class PageSerializer < ActiveModel::Serializer
  attributes :id, :url
  has_many :page_contents

  def page_contents
    object.page_contents.select('id, content, content_type').group_by(&:content_type)
  end
end
