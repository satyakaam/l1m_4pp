## **Readme** ##
## Configuration ##
Make sure you have installed Rails 5 and Ruby 2.2.2+ before proceeding.

    $ git clone <app>
    $ cd <app>
    $ bundle install
    
## Database creation and initialization ##

    $ rake db:create
    $ rake db:migrate

## Usage ##

Before executing any request start the rails server
> rails server

Getting list of stored pages
> Request:
> GET /pages
   

To see an individual page
> Request:
> GET /pages/:id

To create a new page
> Request:
> POST /pages
>    
> Body:
> page[url]: http//:www.my_fancy_url.com

To destroy a page
> Request:
> DELETE /pages/:id

To get the list of all contents

> Request:
> GET /page_contents

To see an individual content

> Request:
> GET /page_contents/:id
